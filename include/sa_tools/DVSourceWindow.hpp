//
// Created by luca on 15/07/17.
//

/**
 * The #DVSourceWindow class is a convenient place to put all the data and functions required to generate the safety
 * area.
 *
 * The idea behind the safety area generation procedure is that whilst the cameras record images, we can draw the
 * safety area simply moving the dvrk arm on the console, whilst pressing the camera pedal and the finger grip.
 */


#pragma once

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <geometry_msgs/Polygon.h>
#include <sensor_msgs/Joy.h>

#include <dvrk_common/camera/simple_camera.hpp>
#include <dvrk_common/opencv/util.hpp>


enum class ButtonState {
    Pressed, Released
};

geometry_msgs::Point32 fitPointToImageSize(const sensor_msgs::Image &img, const float x, const float y);

class DVSourceWindow {
public:
    DVSourceWindow();

    void run();

public: /* Callbacks */
    void onMasterPointReceived(const sensor_msgs::Joy::ConstPtr &pose_msg);

private: /* Member Variables */

    /// Name of the window used to show the safety area image.
    const std::string _window_name = "Define Safety Area";

    ros::NodeHandle _node;
    ros::NodeHandle _pnode;

    /// Subscribes to the cursor data
    ros::Subscriber _master_point;

    /// Publisher to the topic onto which we will push the safety area when it has been defined
    ros::Publisher _sa_publisher;

    /// Camera subscriber from which we pull the images displayed in the SA selection window
    dvrk::SimpleCamera _camera;

    /// The shape of the safety area as it is currently defined.
    /// This is updated in the \c cvMouseEvent callback and contains the current set of selected points
    geometry_msgs::Polygon _safety_area;

    /// The position of the cursor icon as it is currently defined.
    /// This is updated in the \c cvMouseEvent callback and contains the current position of the cursor
    geometry_msgs::Point32 new_image_point;

    /// The finger grip let us to draw the safety area, when pressed with the camera pedal.
    /// If you press the camera pedal you start to publish the cursor position, but you only
    /// draw when you press the finger grip. When the finger grip is released the drawing is finished.
    /// If you press again the finger grip you draw a new drawing whilst the last drawing is cancelled.
    ButtonState _finger_grip_state = ButtonState::Released;
    ButtonState _camera_pedal_state = ButtonState::Released;
};