//
// Created by tibo on 07/07/17.
//

/**
 * The #CVSourceWindow class is a convenient place to put all the data and functions required to generate the safety
 * area.
 *
 * The idea behind the safety area generation procedure is that whilst the cameras record images, we can draw the
 * safety area simply moving the mouse whilst holding the left mouse button pressed.
 */


#pragma once

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <geometry_msgs/Polygon.h>

#include <dvrk_common/camera/simple_camera.hpp>
#include <dvrk_common/opencv/util.hpp>

enum class ButtonState {
    Pressed, Released
};

geometry_msgs::Point32 clampPointToImageSize(const sensor_msgs::Image &img, const int x, const int y);

void cvMouseEventDispatcher(int event, int x, int y, int, void *data);

class CVSourceWindow {
public:
    CVSourceWindow();

    void run();

public: /* Callbacks */
    void cvMouseEvent(int event, int mouse_x, int mouse_y);

private: /* Member Variables */

    /// Name of the window used to show the safety area image.
    const std::string _window_name = "Define Safety Area";

    ros::NodeHandle _node;
    ros::NodeHandle _pnode;

    /// Publisher to the topic onto which we will push the safety area when it has been defined
    ros::Publisher _sa_publisher;

    /// Camera subscriber from which we pull the images displayed in the SA selection window
    dvrk::SimpleCamera _camera;

    /// The shape of the safety area as it is currently defined.
    /// This is updated in the \c cvMouseEvent callback and contains the current set of selected points
    geometry_msgs::Polygon _safety_area;

    /// The state of the left mouse button.
    ButtonState _lbutton_state = ButtonState::Released;

    /// Whether the usey in the process of drawing the safety area or not
    bool _is_drawing = true;
};
