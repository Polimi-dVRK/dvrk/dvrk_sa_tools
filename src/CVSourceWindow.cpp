#include <algorithm>

#include <opencv2/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <cv_bridge/cv_bridge.h>

#include <dvrk_common/math.hpp>
#include <dvrk_common/ros/params.hpp>
#include <dvrk_common/conversions/cv_ros.hpp>

#include "sa_tools/CVSourceWindow.hpp"

/// Constructor
CVSourceWindow::CVSourceWindow()
:   _pnode("~"), _camera(_node)
{
    auto image_topic = dvrk::getRequiredParam<std::string>(_pnode, "image_topic");
    _camera.setCameraTopic(image_topic);

    auto safety_area_topic = dvrk::getRequiredParam<std::string>(_pnode, "safety_area_topic");
    _sa_publisher = _node.advertise<geometry_msgs::Polygon>(safety_area_topic, 1);

    ROS_INFO_STREAM(
            "Safety Area tools ready, read the following configuration: \n"
                    << "    - Reading images from: " << image_topic.c_str() << "\n"
                    << "    - Publishing safety area to: " << safety_area_topic.c_str() << "\n"
    );
}


/**
 * Mouse event handler for the OpenCV window that is drawing the camera image.
 *
 * @param event The mouse event type (ButtonUp, ButtonDown, MouseMove, ...)
 * @param mouse_x The X coordinate of the cursor in the window
 * @param mouse_y The Y coordinate of the cursor in the window
 */
void CVSourceWindow::cvMouseEvent(int event, int mouse_x, int mouse_y)
{
    switch (event) {
        case cv::EVENT_LBUTTONDOWN: {
            _lbutton_state = ButtonState::Pressed;

            // We want to define a new safety area
            _safety_area.points.clear();

            auto new_image_point = clampPointToImageSize(_camera.getLatestImage(), mouse_x, mouse_y);
            _safety_area.points.push_back(new_image_point);

            _is_drawing = true;

            break;
        }
        case cv::EVENT_MOUSEMOVE: {
            // If the left mouse button is pressed then we are defining the safety area. Otherwise we don't have
            // anything to do
            if (_lbutton_state == ButtonState::Released)
                return;

            auto new_image_point = clampPointToImageSize(_camera.getLatestImage(), mouse_x, mouse_y);
            _safety_area.points.push_back(new_image_point);

            break;
        }
        case cv::EVENT_LBUTTONUP: {
            // When we release the left mouse button we have finished drawing the safety area
            _lbutton_state = ButtonState::Released;
            _sa_publisher.publish(_safety_area);

            _is_drawing = false;

            break;
        }
        default:
            break;
    }
}


/**
 * Main loop.
 *
 * In this loop we draw the camera image and the current safety area.
 */
void CVSourceWindow::run()
{
    // Wait until we have received an image before opening the window
    if (!_camera.waitForFirstImage(5.0)) {
        ROS_ERROR("Timeout whilst waiting for images on <%s>. Exiting ... ", _camera.getTopic().c_str());
        ros::shutdown();
        exit(-1);
    }

    // We have received an image, show the window and start working
    cv::namedWindow(_window_name, cv::WINDOW_NORMAL);
    cv::setMouseCallback(_window_name, cvMouseEventDispatcher, this);

    while (ros::ok()) {
        ros::spinOnce();

        auto cv_image = cv_bridge::toCvCopy(_camera.getLatestImage(), "bgr8");

        // Draw the safety area on the image
        for (size_t i = 1; i < _safety_area.points.size(); i++) {
            auto p1 = dvrk::toPoint2f(_safety_area.points.at(i-1));
            auto p2 = dvrk::toPoint2f(_safety_area.points.at(i));
            cv::line(cv_image->image, p1, p2, cv::Scalar(0, 0, 255), 3, cv::LINE_AA);
        }

        // If the user has finished drawing then we want close the area they selected by visually connecting the first
        // and last point of the line
        if (!_is_drawing && _lbutton_state == ButtonState::Released) {
            auto first = dvrk::toPoint2f(_safety_area.points.front());
            auto last = dvrk::toPoint2f(_safety_area.points.back());
            cv::line(cv_image->image, first, last, cv::Scalar(0, 0, 255), 3, cv::LINE_AA);
        }

        cv::imshow(_window_name, cv_image->image);

        const auto key = static_cast<char>(cv::waitKey(1));
        dvrk::opencvKeyboardHandler(_window_name, key);
    }

    cv::destroyWindow(_window_name);
}


/**
 * Convert a position to a \c geometry_msgs::Point32 and clamp it to the size of the image. The \c Z coordinate is
 * set to 0.
 *
 * @param img The image who's size we should consider
 * @param x The X coordinate
 * @param y The Y coordinate
 * @return A \c geometry_msgs::Point32 that is guaranteed to be part of the image
 */
geometry_msgs::Point32 clampPointToImageSize(const sensor_msgs::Image &img, const int x, const int y)
{
    geometry_msgs::Point32 new_point;
    new_point.x = static_cast<float>(dvrk::clamp<int>(x, 0, img.width-1));
    new_point.y = static_cast<float>(dvrk::clamp<int>(y, 0, img.height-1));
    new_point.z = 0.0f;

    return new_point;
}


/**
 * Workaround for the fact that we can't set the OpenCV mouse event callback to a class method. Instead we use a free
 * function and pass a pointer to the class in the extra data member.
 *
 * @param event The type of event
 * @param x The X coordinate of the mouse cursor in the window
 * @param y The Y coordinate of the mouse cursor in the window
 * @param data Extra data to pass to the callback. We use it to store the pointer to the class instance
 */
void cvMouseEventDispatcher(int event, int x, int y, int, void *data)
{
    ROS_ASSERT(data != nullptr);
    static_cast<CVSourceWindow*>(data)->cvMouseEvent(event, x, y);
}
