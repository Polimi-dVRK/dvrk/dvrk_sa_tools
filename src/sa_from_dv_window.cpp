//
// Created by luca on 15/07/17.
//

/**
 * Define a safety area using the dvrk arm on the console.
 *
 * The image from one of the cameras is shown in an OpenCV window. Use the dvrk arm to draw the safety area on the image
 * by pressing the finger grip and the pedal and dragging until you have drawn the desired shape.
 *
 * See \sa CVSourceWindow for more information.
 */

#include "sa_tools/DVSourceWindow.hpp"

int main(int argc, char** argv) {
    ros::init(argc, argv, "sa_from_dv_window");

    DVSourceWindow dv;
    dv.run();
}
