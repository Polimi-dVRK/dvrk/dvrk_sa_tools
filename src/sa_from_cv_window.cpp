
/**
 * Define a safety area using the mouse.
 *
 * The image from one of the cameras is shown in an OpenCV window. Use your mouse to draw the safety area on the image
 * by pressing the left mouse button and dragging until you have drawn the desired shape.
 *
 * See \sa CVSourceWindow for more information.
 */

#include "sa_tools/CVSourceWindow.hpp"

int main(int argc, char** argv) {
    ros::init(argc, argv, "sa_from_cv_window");

    CVSourceWindow cv;
    cv.run();
}
