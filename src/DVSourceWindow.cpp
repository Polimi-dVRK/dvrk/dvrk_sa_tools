//
// Created by luca on 15/07/17.
//

#include <algorithm>

#include <opencv2/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <cv_bridge/cv_bridge.h>

#include <dvrk_common/ros/params.hpp>
#include <dvrk_common/conversions/cv_ros.hpp>

#include "sa_tools/DVSourceWindow.hpp"

/// Constructor
DVSourceWindow::DVSourceWindow()
:   _pnode("~"), _camera(_node)
{
    auto image_topic = dvrk::getRequiredParam<std::string>(_pnode, "image_topic");
    _camera.setCameraTopic(image_topic);

    auto cursor_topic = dvrk::getRequiredParam<std::string>(_pnode, "cursor_topic");
    _master_point = _node.subscribe(cursor_topic, 1, &DVSourceWindow::onMasterPointReceived, this);

    auto safety_area_topic = dvrk::getRequiredParam<std::string>(_pnode, "safety_area_topic");
    _sa_publisher = _node.advertise<geometry_msgs::Polygon>(safety_area_topic, 1);

    ROS_INFO_STREAM(
            "Safety Area tools ready, read the following configuration: \n"
                    << "    - Reading images from: " << image_topic.c_str() << "\n"
                    << "    - Reading cursor information from: " << cursor_topic.c_str() << "\n"
                    << "    - Publishing safety area to: " << safety_area_topic.c_str() << "\n"
    );
}


/**
 * Callback called when new cursor position messages are received
 *
 * This callback creates the safety area and publishes the it to the specified topic when it is complete.
 *
 * @param point_msg
 *      A pointer to the sensor_msgs::Joy containing the cursor information
 */
void DVSourceWindow::onMasterPointReceived(const sensor_msgs::Joy::ConstPtr &point_msg) {

    if (!point_msg->buttons.empty()) {
        _camera_pedal_state = ButtonState::Pressed;

        new_image_point = fitPointToImageSize(_camera.getLatestImage(), point_msg->axes[0], point_msg->axes[1]);

        if (point_msg->buttons[0] && _finger_grip_state == ButtonState::Released) {

            // We want to define a new safety area
            _safety_area.points.clear();
            _safety_area.points.push_back(new_image_point);

            _finger_grip_state = ButtonState::Pressed;

        } else if (point_msg->buttons[0] && _finger_grip_state == ButtonState::Pressed) {
            _safety_area.points.push_back(new_image_point);

        } else if (!point_msg->buttons[0] && _finger_grip_state == ButtonState::Pressed) {
            _sa_publisher.publish(_safety_area);

            // When we release the left mouse button we have finished drawing the safety area
            _finger_grip_state = ButtonState::Released;

        }

    } else {
        _camera_pedal_state = ButtonState::Released;
    }
}


/**
 * Main loop.
 *
 * In this loop we draw the camera image and the current safety area.
 */
void DVSourceWindow::run()
{
    // Wait until we have received an image before opening the window
    if (!_camera.waitForFirstImage(5.0)) {
        ROS_ERROR("Timeout whilst waiting for images on <%s>. Exiting ... ", _camera.getTopic().c_str());
        ros::shutdown();
        exit(-1);
    }

    // We have received an image, show the window and start working
    cv::namedWindow(_window_name, cv::WINDOW_NORMAL);

    while (ros::ok()) {
        ros::spinOnce();

        auto cv_image = cv_bridge::toCvCopy(_camera.getLatestImage(), "bgr8");
        ROS_ASSERT(cv_image->image.cols == _camera.getLatestImage().width);
        ROS_ASSERT(cv_image->image.rows == _camera.getLatestImage().height);

        // Draw the safety area on the image
        for (size_t i = 1; i < _safety_area.points.size(); i++) {
            auto p1 = dvrk::toPoint2f(_safety_area.points.at(i-1));
            auto p2 = dvrk::toPoint2f(_safety_area.points.at(i));
            cv::line(cv_image->image, p1, p2, cv::Scalar(0, 0, 255), 3, cv::LINE_AA);
        }

        // If the user has finished drawing then we want close the area they selected by visually connecting the first
        // and last point of the line
        if (_camera_pedal_state == ButtonState::Released && _finger_grip_state == ButtonState::Released) {
            auto first = dvrk::toPoint2f(_safety_area.points.front());
            auto last = dvrk::toPoint2f(_safety_area.points.back());
            cv::line(cv_image->image, first, last, cv::Scalar(0, 0, 255), 3, cv::LINE_AA);
        }

        // Draw the cursor
        if (_camera_pedal_state == ButtonState::Pressed) {
            cv::circle(cv_image->image, dvrk::toPoint2f(new_image_point), 5, cv::Scalar(0, 255, 0), CV_FILLED);
        }

        cv::imshow(_window_name, cv_image->image);

        const auto key = static_cast<char>(cv::waitKey(1));
        dvrk::opencvKeyboardHandler(_window_name, key);
    }

    cv::destroyWindow(_window_name);
}


/**
 * Convert a position to a \c geometry_msgs::Point32 and fit to the size of the image. The \c Z coordinate is
 * set to 0.
 *
 * @param img The image who's size we should consider
 * @param x The X coordinate
 * @param y The Y coordinate
 * @return A \c geometry_msgs::Point32 that is guaranteed to be part of the image
 */
geometry_msgs::Point32 fitPointToImageSize(const sensor_msgs::Image &img, const float x, const float y)
{
    geometry_msgs::Point32 new_point;
    new_point.x = ((x + 1)*(img.width-1))/2;
    new_point.y = ((y + 1)*(img.height-1))/2;
    new_point.z = 0.0f;

    return new_point;
}