cmake_minimum_required(VERSION 2.8.3)
project(sa_tools)
set(CMAKE_CXX_STANDARD 14)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED
        cv_bridge
        geometry_msgs
        dvrk_common
        )

# Opencv required for window handling and input
find_package(OpenCV REQUIRED)

###################################
## catkin specific configuration ##
###################################
catkin_package()

###########
## Build ##
###########

include_directories(include ${catkin_INCLUDE_DIRS})

add_executable(sa_from_cv_window
        src/sa_from_cv_window.cpp
        src/CVSourceWindow.cpp
        )
target_link_libraries(sa_from_cv_window ${OpenCV_LIBRARIES} ${catkin_LIBRARIES})

add_executable(sa_from_dv_window
        src/sa_from_dv_window.cpp
        src/DVSourceWindow.cpp
        )
target_link_libraries(sa_from_dv_window ${OpenCV_LIBRARIES} ${catkin_LIBRARIES})

#############
## Install ##
#############

## Mark executables and/or libraries for installation
install(TARGETS sa_from_cv_window sa_from_dv_window
        ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
        LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
        RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
        )

# Dummy target for CLion
FILE (GLOB_RECURSE clion_all_headers
        ${CMAKE_SOURCE_DIR}/**/*.hpp
        ${CMAKE_SOURCE_DIR}/**/*.h)
ADD_CUSTOM_TARGET(all_clion SOURCES ${clion_all_headers})

install(DIRECTORY launch/
        DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}/launch
        FILES_MATCHING PATTERN "*.launch"
        )

