
# Safety Area Tools

The safety area tools are part of the
[enVisors](https://gitlab.com/Polimi-dVRK/envisors_system) project.
The nodes in this package provide different way to draw the safety area
on the camera images.

### Using a Mouse

The `sa_from_cv_input` node shows the camera images and lets the user
select the adafety area by simply drawing it on the images with the
mouse.

### Using the dVRK master arms as pointers

The dVRK is the daVinci Research Kit, a set of hardware controllers and
associated software that give direct access to the hardware of an early
generation Intuitive Surgical daVinci robot.

The [dvkr_pointer](https://gitlab.com/Polimi-dVRK/dvrk/dvrk_pointer)
node developped at Nearlab allows us to use the master side arms of the
robot as a virtual mouse. The `sa_from_dv_window` node uses this feature
to allow the user to directlyy select the safety area.

### Dependencies

These nodes both depend on the
[dvrk_common](https://gitlab.com/Polimi-dVRK/dvrk/dvrk_common) package.
The `sa_from_dv_window` node additionally depends on the
[dvrk_pointer](https://gitlab.com/Polimi-dVRK/dvrk/dvrk_pointer)
package.